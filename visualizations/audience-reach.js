/**
 * Deps:
 *  - https://code.jquery.com/jquery-3.3.1.min.js
 *  - https://dino-looker.gitlab.io/looker-custom-visualizations/libs/gauge-meter.js
 *  - https://cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js
 */

// TODO: Make sure gauge has unique ID
// TODO: implement min and max range

/**
 * returns vis looker option or the fallback
 * 
 * @param {object} options the vis options
 * @param {object} config the looker config
 * @param {string} name the name of the option
 */
function getOption(options, config, name) {
    var defaultValue = options[name].default;
    return config ? config[name] || defaultValue : defaultValue;
}

/**
 * returns the vis options for the gauge
 * 
 * @param {object} options the vis options
 * @param {object} config the looker config
 */
function getSettings(options, config) {
    return ({
        size: getOption(options, config, 'size'),
        archWidth: getOption(options, config, 'arch_width'),
        archFrontColor: getOption(options, config, 'arch_front_color'),
        archBackColor: getOption(options, config, 'arch_back_color'),
        archGradient: getOption(options, config, 'arch_gradient'),
        countLabelSize: getOption(options, config, 'count_label_size'),
        countLabelColor: getOption(options, config, 'count_label_color'),
        textLabelSize: getOption(options, config, 'text_label_size'),
        textLabelColor: getOption(options, config, 'text_label_color'),
        textErrorSize: getOption(options, config, 'text_error_size'),
        textErrorColor: getOption(options, config, 'text_error_color'),
        min_range: getOption(options, config, 'min_range'),
        min_range_type: getOption(options, config, 'min_range_type')
    });
}

/**
 * draw the gauge with the given settings and params
 * 
 * @param {object} settings the vis settings
 * @param {object} extraParams extra params to pass to the gauge
 */
function drawGauge(settings, extraParams) {
    // setup gauge options
    var gaugeOptions = {
        min: 0,
        size: settings.size,
        color: settings.archFrontColor,
        back: settings.archBackColor,
        width: settings.archWidth,
        style: 'Arch',
        stripe: 0,
        animationstep: 1,
        animate_gauge_colors: true,
        animate_text_colors: true,
        fill: 'rgba(0,0,0,0)',
        showvalue: true,
        gradient: settings.archGradient.join()
    }

    // draw
    $("#audience-reach-gauge").gaugeMeter({ ...gaugeOptions, ...(extraParams || {}) });
}

function checkValidReach(settings, count, total) {
    var min = settings.min_range;
    var min_type = settings.min_range_type;

    if (min_type === 'FIXED') {
        if (count < min) { return false }
    } else {
        if (count < (total / 100 * min)) { return false }
    }

    return true;
}

function getCountString (count) {
    for (let i = 1000; i <= 1000000000; i = i * 1000) {
        let divided = count / i;
        if (divided < 10 && divided > 1) {
            let result = Math.floor(divided * 10) / 10 * i;
            return `> ${numeral(result).format('0.0a')}`;
        }
    }
    return `> ${Math.floor(count / 100) * 100}`;
}

looker.plugins.visualizations.add({
    id: 'cake_audience_reach',
    label: 'Audience Reach',
    options: {
        min_range: {
            section: 'Range Limits',
            order: 1,
            type: 'number',
            label: 'Minimum Range Size',
            display: 'number',
            default: 5,
            min: 0,
            step: 1
        },
        min_range_type: {
            section: 'Range Limits',
            order: 2,
            type: 'string',
            label: 'Min. Type',
            values: [
                { PERCENTAGE: 'PERCENTAGE' },
                { FIXED: 'FIXED' }
            ],
            display: "select",
            default: "PERCENTAGE"
        },
        size: {
            section: 'Arch',
            order: 1,
            type: 'number',
            display: 'number',
            label: 'Size (px)',
            default: 300
        },
        arch_width: {
            section: 'Arch',
            order: 2,
            type: 'number',
            display: 'number',
            label: 'Arch width (px)',
            default: 40
        },
        arch_front_color: {
            section: 'Arch',
            order: 3,
            type: 'string',
            display: 'color',
            label: 'Arch front color',
            default: '#4681F7'
        },
        arch_back_color: {
            section: 'Arch',
            order: 4,
            type: 'string',
            display: 'color',
            label: 'Arch back color',
            default: '#EBF2F5'
        },
        arch_gradient: {
            section: 'Arch',
            order: 5,
            type: 'array',
            display: 'colors',
            label: 'Arch gradient colors',
            default: ['#48DFFB', '#4681F7']
        },
        count_label_size: {
            section: 'Labels',
            order: 1,
            type: 'number',
            display: 'number',
            label: 'Count font size',
            default: 50
        },
        count_label_color: {
            section: 'Labels',
            order: 2,
            type: 'string',
            display: 'color',
            label: 'Count font color',
            default: '#30405D'
        },
        text_label_size: {
            section: 'Labels',
            order: 3,
            type: 'number',
            display: 'number',
            label: 'Label font size',
            default: 20
        },
        text_label_color: {
            section: 'Labels',
            order: 4,
            type: 'string',
            display: 'color',
            label: 'Label font color',
            default: '#979FAE'
        },
        text_error_size: {
            section: 'Labels',
            order: 5,
            type: 'number',
            display: 'number',
            label: 'Error font size',
            default: 20
        },
        text_error_color: {
            section: 'Labels',
            order: 6,
            type: 'string',
            display: 'color',
            label: 'Error font color',
            default: '#30405D'
        }
    },

    // Set up the initial state of the visualization
    create: function (element, config) {
        this.style = document.createElement('style')
        document.head.appendChild(this.style)

        // create a container element to let us center the text.
        this.container = element.appendChild(document.createElement('div'));
        this.container.className = 'audience-reach-vis';

        // create container for the gauge
        this._gaugeElement = document.createElement('div');
        this._gaugeElement.className = 'audience-reach-gauge';
        this._gaugeElement.id = 'audience-reach-gauge'

        // create an element to contain all the text.
        this._innerText = document.createElement('div');
        this._innerText.className = 'audience-reach-inner-text';

        // create an element for the count label
        this._countWrapper = document.createElement('div');
        this._countWrapper.className = 'audience-reach-count';

        // create an element for the count info label
        this._labelWrapper = document.createElement('div');
        this._labelWrapper.className = 'audience-reach-label';
        this._labelWrapper.innerHTML = 'Users';

        // create an element for the count label
        this._errorWrapper = document.createElement('div');
        this._errorWrapper.className = 'audience-reach-inner-error';
        
        // add the gauge to the container
        this.container.appendChild(this._gaugeElement);

        // add the labels to the text element
        this._innerText.appendChild(this._countWrapper);
        this._innerText.appendChild(this._labelWrapper);

        // add the text to the container
        this.container.append(this._innerText);

        // add the error wrapper to the container
        this.container.append(this._errorWrapper);

        // initial draw
        drawGauge(getSettings(this.options, config));

        // max value
        this.maxValue = null;

    },

    // Render in response to the data or settings changing
    updateAsync: function (data, element, config, queryResponse, details, done) {
        // Clear any errors from previous updates
        this.clearErrors();

        if (data && data.length) {
            // rerender when we have new settings or new result
            var newSettings = getSettings(this.options, config);
            var customerCount = null;

            for (const [_, dataObj] of Object.entries(data[0])) {
                // assign result values
                if (this.maxValue === null) {
                    this.maxValue = dataObj.value
                } else {
                    customerCount = dataObj.value
                }

                // reorder result values
                if (this.maxValue < customerCount) {
                    [this.maxValue, customerCount] = [customerCount, this.maxValue];
                }
            }
    
            var shouldUpdate = (
                JSON.stringify(this.settings) !== JSON.stringify(newSettings)
                || this.currentCount !== customerCount
            );
    
            // update the count when updated
            if (shouldUpdate) {
                // create an option dict based on config en defaults
                this.settings = newSettings;
    
                // show/hide error and data
                var isValid = checkValidReach(this.settings, customerCount, this.maxValue);
                this._innerText.style.display = isValid !== true ? 'none' : 'block';
                this._errorWrapper.style.display = !isValid ? 'block' : 'none';
                this._errorWrapper.innerHTML = isValid ? '' : 'Target audience too narrow';
    
                // set the elements height
                element.style.height = `${this.settings.size - 50}px`;
    
                // inject our styling
                this.style.innerHTML = `
                    .audience-reach-vis {
                        height: ${this.settings.size - 50}px;
                        display: flex;
                        flex-direction: column;
                        justify-content: flex-start;
                        text-align: center;
                        position: relative;
                        align-items: center;
                        font-family: "Circular", sans-serif;
                        overflow: hidden;
                    }
                    .audience-reach-gauge span,
                    .audience-reach-gauge b {
                        display: none;
                    }
                    .audience-reach-inner-text,
                    .audience-reach-inner-error {
                        position: absolute;
                        width: 100%;
                        top: calc(50% - 15px);
                    }
                    .audience-reach-inner-error {
                        width: ${this.settings.size}px;
                        padding: 20px 70px;
                        box-sizing: border-box;
                        font-weight: 800;
                        color: ${this.settings.textErrorColor};
                        font-size: ${this.settings.textErrorSize}px;
                    }
                    .audience-reach-inner-text .audience-reach-count {
                        color: ${this.settings.countLabelColor};
                        font-size: ${this.settings.countLabelSize}px;
                    }
                    .audience-reach-inner-text .audience-reach-label {
                        color: ${this.settings.textLabelColor};
                        font-size: ${this.settings.textLabelSize}px;
                    }
                `;
    
                this.currentCount = customerCount
                this._countWrapper.innerHTML = getCountString(customerCount);
                drawGauge(this.settings, { used: customerCount, total: this.maxValue });
            }
        }

        // We are done rendering! Let Looker know.
        done();
    }
});